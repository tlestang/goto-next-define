(defun count-match-in-region (regexp start end)
  "Count number of matches for REGEXP between two positions START
and END."
  (save-excursion
    (let ((count 0))
      (goto-char start)
      (while (re-search-forward regexp end t)
	(setq count (+ count 1)))
      count)))

(defun goto-close-tag (&optional count)
  (setq count (or count 1))
  (when (> count 0)
    (let ((beg (point)))
      (goto-close-tag (count-match-in-region
		 "^#if" beg
		 (re-search-forward "^#endif" nil t count))))))

(defun goto-open-tag (&optional count)
  (setq count (or count 1))
  (when (> count 0)
    (let ((beg (point)))
      (goto-open-tag
       (count-match-in-region
	"^#endif"
	(re-search-backward "^#if" nil t count) beg)))))

(defun jump-to-tag (arg)
  "Set point to the beginning of the line containing the next #if
preprocessor directive.  If point is on a line starting with a
#if directive, set point to the corresponding #endif directive.
Conversely, set point to corresponding opening #if directive if
point on a line with a #endif directive."
  (interactive "P")
  (beginning-of-line)
  (cond ((looking-at "^#endif")
	 (goto-open-tag))
	((looking-at "^#if")
	 (progn
	   (re-search-forward "^#if")
	   (goto-close-tag)
	   (beginning-of-line)))
	(t (progn
	     (re-search-forward "^#if" nil nil (when arg -1))
	     (beginning-of-line)))))
